# USAGE
# python ocr_license_plate.py --input license_plates/group1
# python ocr_license_plate.py --input license_plates/group2 --clear-border 1

# import the necessary packages
from pyimagesearch.anpr import PyImageSearchANPR
from imutils import paths
import argparse
import imutils
import cv2
import pandas as pd
import re

def cleanup_text(text):
	# strip out non-ASCII text so we can draw the text on the image
	# using OpenCV
	return "".join([c if ord(c) < 128 else "" for c in text]).strip()

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
	help="path to input directory of images")
ap.add_argument("-c", "--clear-border", type=int, default=-1,
	help="whether or to clear border pixels before OCR'ing")
ap.add_argument("-p", "--psm", type=int, default=7,
	help="default PSM mode for OCR'ing license plates")
ap.add_argument("-d", "--debug", type=int, default=-1,
	help="whether or not to show additional visualizations")
args = vars(ap.parse_args())

# initialize our ANPR class
anpr = PyImageSearchANPR(debug=args["debug"] > 0)

# grab all image paths in the input directory
imagePaths = sorted(list(paths.list_images(args["input"])))

plates = []

# loop over all image paths in the input directory
for imagePath in imagePaths:
	# load the input image from disk and resize it
	image = cv2.imread(imagePath)
	image = imutils.resize(image, width=600)

	# apply automatic license plate recognition
	(lpText, lpCnt) = anpr.find_and_ocr(image, psm=args["psm"],
		clearBorder=args["clear_border"] > 0)

	# only continue if the license plate was successfully OCR'd
	if lpText is not None and lpCnt is not None:
		# fit a rotated bounding box to the license plate contour and
		# draw the bounding box on the license plate
		box = cv2.boxPoints(cv2.minAreaRect(lpCnt))
		box = box.astype("int")
		cv2.drawContours(image, [box], -1, (0, 255, 0), 2)

		# compute a normal (unrotated) bounding box for the license
		# plate and then draw the OCR'd license plate text on the
		# image
		(x, y, w, h) = cv2.boundingRect(lpCnt)
		cv2.putText(image, cleanup_text(lpText), (x, y - 15),
			cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)

		# show the output ANPR image
		print("[INFO] {}".format(lpText))
		print(lpText)
		cv2.imshow("Output ANPR", image)
		cv2.waitKey(0)
	
	# save the plate into the list
	plates.append(lpText)

plates_cleaned = []
for plate in plates:
	plate = re.sub('\n\x0c', '', plate)
	plates_cleaned.append(plate)

plates = plates_cleaned
print(plates)








# # Save the DataFrame as CSV
# def saveToCSV(key, plate):

#     ## Comment Parents
#     df = pd.DataFrame()
#     for index, row in plate.iterrows():
#         data = ct.parentsDataFrame(key, row['videoId'])
#         df = pd.concat([data, df])
    
#     ct.dataFrameToCSV(df, True)
    
#     ## Comment Replies
#     df = pd.DataFrame()
#     for index, row in plate.iterrows():
#         data = ct.repliesDataFrame(key, row['videoId'])
#         df = pd.concat([data, df])

#     ct.dataFrameToCSV(df, False)