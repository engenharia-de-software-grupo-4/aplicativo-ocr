import mysqlFunctions as mysqlf
from datetime import datetime

dic = {
    "hostname": "localhost",
    "user": "admin",
    "passwd": "Orion123",
    "database": "stolen_cars_db"
}

def verificaRodizio(placa):
    tamanho = len(placa)
    final_placa = placa[tamanho - 1]
    final_placa = int(final_placa)
    dia_de_hoje = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print("Dia/Hora: " + dia_de_hoje)
    numero_do_dia = datetime.today().isoweekday()
    if(final_placa == numero_do_dia):
        return True
    else:
        return False

def ifIsStolen(placa):
    statement="SELECT placa FROM stolen_cars WHERE placa='{}'".format(placa)
    return True if placa in mysqlf.callTableSelect(dic["hostname"],
            dic["user"],
            dic["passwd"],
            dic["database"],
            statement) else False


def writePlate(placa):
    statement1="INSERT INTO detected_cars (PLACA) VALUES ('{}')".format(placa)
    mysqlf.callTableInsert(dic["hostname"],
                    dic["user"],
                    dic["passwd"],
                    dic["database"],
                    statement1)
    
    statement2="SELECT placa FROM detected_cars WHERE placa='{}'".format(placa)
    return True if placa in mysqlf.callTableSelect(dic["hostname"],
        dic["user"],
        dic["passwd"],
        dic["database"],
        statement2) else False

