from tkinter import *
import rules
from ocr_license_plate import plate_detection
from datetime import datetime


def platePath():
    # funções
    plate = plate_detection(pasta.get())
    verificaRodizio = rules.verificaRodizio(plate[0])
    verificaRoubo = rules.ifIsStolen(plate[0])
    rules.writePlate(plate[0])

    # prints
    placa['text']=plate
    rodizio['text'] = "Sim" if (verificaRodizio==True) else "Não"
    status['text'] = "Sim" if (verificaRoubo==True) else "Não"
    data['text'] = datetime.now().strftime('%Y-%m-%d')


app=Tk()
app.title("Detecção de Placas")
app.geometry("300x600")
app.configure(background="#edd14b")

# anchor=> N=Norte, S=Sul, E=Leste, W=Oeste
# NE=Nordeste, SE=Sudeste, SO=Sudoeste, NO=Noroeste
lab1=Label(app,text="Foto do veículo",background="#edd14b",foreground="#000",anchor=W)
lab1.place(x=10,y=50,width=105,height=20)

pasta=Entry(app,background="#e4b138")
pasta.place(x=120,y=50,width=150,height=20)

lab2=Label(app,text="Placa: ",background="#edd14b",foreground="#000",anchor=W)
lab2.place(x=10,y=100,width=100,height=20)
placa=Label(app,text="-",background="#edd14b",foreground="#000",anchor=W)
placa.place(x=120,y=100,width=100,height=20)

lab3=Label(app,text="Rodízio: ",background="#edd14b",foreground="#000",anchor=W)
lab3.place(x=10,y=130,width=100,height=20)
rodizio=Label(app,text="-",background="#edd14b",foreground="#000",anchor=W)
rodizio.place(x=120,y=130,width=100,height=20)


lab4=Label(app,text="Roubado: ",background="#edd14b",foreground="#000",anchor=W)
lab4.place(x=10,y=160,width=100,height=20)
status=Label(app,text="-",background="#edd14b",foreground="#000",anchor=W)
status.place(x=120,y=160,width=100,height=20)

lab5=Label(app,text="Data: ",background="#edd14b",foreground="#000",anchor=W)
lab5.place(x=10,y=190,width=100,height=20)
data=Label(app,text="-",background="#edd14b",foreground="#000",anchor=W)
data.place(x=120,y=190,width=100,height=20)

btn=Button(app,text="Enviar",background="#e4b138",command=platePath)
btn.place(x=40,y=500,width=100,height=20)

# Button for closing
exit_button = Button(app, text="Exit",background="#e4b138", command=app.destroy)
exit_button.place(x=150,y=500,width=100,height=20)
# exit_button.pack(pady=20)

app.mainloop()