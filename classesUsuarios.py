class usuario:
    def __init__(self, nome, idade, cpf, senha): 
        self.nome = nome 
        self.idade = idade
        self.cpf = cpf
        self.senha = senha
    def setNome(self, nome): self.nome = nome 
    def setIdade(self, idade): self.idade = idade 
    def setCpf(self, cpf): self.cpf = cpf
    def setSenha(self, senha): self.senha = senha
    def getNome(self): return self.nome 
    def getIdade(self): return self.idade
    def getCpf(self): return self.cpf
    def getSenha(self): return self.senha
    
class adm(usuario):
    def __init__(self, nome, idade, cpf, senha, codigoAdm): 
        super().__init__(nome,idade, cpf, senha) 
        self.codigoAdm = codigoAdm
    def setCodigoAdm(self, codigoAdm): self.codigoAdm = codigoAdm
    def getCodigoAdm(self): return self.codigoAdm
    
class agente(usuario):
    def __init__(self, nome, idade, cpf, senha, registroAgente, filial):
        super().__init__(nome,idade, cpf, senha)
        self.registroAgente = registroAgente
        self.filial = filial
    def setRegistroAgente(self, registroAgente): self.registroAgente = registroAgente
    def setFilial(self, filial): self.filial = filial   
    def getRegistroAgente(self): return self.registroAgente
    def getFilial(self): return self.filial

rafael = adm("Rafael", 24, 419059, 12345, 56)
jose = agente("José", 23, 237144, 12345, 22, "Santo André")

print("Nome:") 
print(rafael.getNome())
print("Idade:")
print(rafael.getIdade())

    

















