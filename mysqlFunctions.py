import mysql.connector
from mysql.connector import errorcode

class connectionDB():
    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self,hostname,user,passwd,database):
        self.connection = mysql.connector.connect(host=hostname, user=user, password=passwd, database=database)
        self.cursor = self.connection.cursor()
    
    def runSQLSelect(self,statement):
        self.cursor.execute(statement)
        return self.cursor.fetchall()
    
    def runSQLInsert(self,statement):
        self.cursor.execute(statement)
        self.connection.commit()
        
def callTableSelect(hostname,user,passwd,database,statement):
    
    instance = connectionDB()
    instance.connect(hostname,user,passwd,database) 
    select = instance.runSQLSelect(statement)
    return [item for t in select for item in t]


def callTableInsert(hostname,user,passwd,database,statement):
    
    instance = connectionDB()
    instance.connect(hostname,user,passwd,database)
    select = instance.runSQLInsert(statement)
    return select